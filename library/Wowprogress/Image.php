<?php
	/**
	 * WowProgress
	 * =============================================================================
	 *
	 * @author		Kalle Sommer Nielsen 			 <kalle@php.net>
	 * @version		1.6
	 * @copyright		Tuxxedo Software Development 2006+
	 * @license		Apache License, Version 2.0
	 * @package		FOSS
	 * @subpackage		WowProgress
	 *
	 * =============================================================================
	 */


	/**
	 * WowProgress namespace
	 *
	 * This namespace contains all the routines for the Wowprogress data collector 
	 * utility set.
	 *
	 * @author		Kalle Sommer Nielsen <kalle@php.net>
	 * @version		1.6
	 * @package		FOSS
	 * @subpackage		WowProgress
	 */
	namespace Wowprogress;


	/**
	 * Image handler class, this class makes it possible to generate 
	 * progression images.
	 *
	 * This class uses LibGD internally, which must be enabled and loaded 
	 * prior to usage. Possible formats depends on what functions are 
	 * available in your installation.
	 *
	 * Further more, PHP must be compiled with FreeType support, or at least 
	 * TrueType writing (fallback):
	 *
	 *    imageft[bbox|text]() > imagettf[bbox|text]()
	 *
	 *
	 * Output formats possible:
	 *   . JPEG
	 *   . PNG
	 *   . GIF
	 *   . XBM
	 *   . WBMP
	 *   . GD
	 *   . GD2
	 *
	 * Input formats possible:
	 *   . JPEG 
	 *   . PNG
	 *   . GIF
	 *   . XBM
	 *   . XPM
	 *   . WBMP
	 *   . GD
	 *   . GD2
	 *
	 * Note, some of these formats are not supported by all browsers, if any and 
	 * are only there because GD supports it. For example, XPM is not supported 
	 * on Windows systems when attempting to load it.
	 *
	 * The recommeded default format is PNG.
	 *
	 * @author		Kalle Sommer Nielsen <kalle@php.net>
	 * @version		1.6
	 * @package		FOSS
	 * @subpackage		WowProgress
	 */
	class Image
	{
		/**
		 * Image type constant - JPEG
		 *
		 * @var		integer
		 */
		const TYPE_JPEG				= 1;

		/**
		 * Image type constant - PNG
		 *
		 * @var		integer
		 */
		const TYPE_PNG				= 2;

		/**
		 * Image type constant - GIF
		 *
		 * @var		integer
		 */
		const TYPE_GIF				= 3;

		/**
		 * Image type constant - XBM
		 *
		 * @var		integer
		 */
		const TYPE_XBM				= 4;

		/**
		 * Image type constant - XPM
		 *
		 * @var		integer
		 */
		const TYPE_XPM				= 5;

		/**
		 * Image type constant - WBMP
		 *
		 * @var		integer
		 */
		const TYPE_WBMP				= 6;

		/**
		 * Image type constant - GD
		 *
		 * @var		integer
		 */
		const TYPE_GD				= 7;

		/**
		 * Image type constant - GD2
		 *
		 * @var		integer
		 */
		const TYPE_GD2				= 8;

		/**
		 * Position constant - Top left
		 *
		 * @var		integer
		 */
		const POS_TOP_LEFT			= 1;

		/**
		 * Position constant - Top right
		 *
		 * @var		integer
		 */
		const POS_TOP_RIGHT			= 2;

		/**
		 * Position constant - Center
		 *
		 * @var		integer
		 */
		const POS_CENTER			= 3;

		/**
		 * Position constant - Bottom left
		 *
		 * @var		integer
		 */
		const POS_BOTTOM_LEFT			= 4;

		/**
		 * Position constant - Bottom right
		 *
		 * @var		integer
		 */
		const POS_BOTTOM_RIGHT			= 5;


		/**
		 * Guild information struct, including tier progressions
		 * this is only for the guild information in \Wowprogress\Image::draw().
		 *
		 * @var		\Wowprogress\Guild
		 */
		protected $guild;

		/**
		 * Progression data for the specific raid
		 *
		 * @var		\stdClass
		 */
		protected $raid;

		/**
		 * Image pointer
		 *
		 * @var		resource
		 */
		protected $ptr;

		/**
		 * The allocated colors
		 *
		 * @var		array
		 */
		protected $colors			= [];


		/**
		 * Constructs a new progression image object
		 *
		 * The constructor takes a specific raid as its first parameter when 
		 * loading and then a background image:
		 *
		 * <code>
		 * $guild = new \Wowprogress\Guild(WowProgress::REGION_EU, 'Twisting Nether', 'Method', '15');
		 * $img = new \Wowprogress\Image($guild, $guild->t15, './images/raid_tot.png');
		 *
		 * ...
		 *
		 * $img->save('./images/raid_tot_progress.png');
		 * </code>
		 *
		 * Note, by omitting the type parameters, the class will fallback to use 
		 * PNG as input and output formats, see above class comment for more detailed 
		 * information on supported formats.
		 *
		 * Note, if a background image fails to load, it can often be because a wrong 
		 * type was specified.
		 *
		 * This also loads the following colors:
		 *
		 *  . black:	0x00 0x00 0x00
		 *  . white:	0xFF 0xFF 0xFF
		 *
		 * @param	\Wowprogress		The guild object
		 * @param	\stdClass		The raid object
		 * @param	string			The path to the image used as background
		 * @param	integer			The image type for the background
		 *
		 * @throws	\Wowprogress\Exception	Throws an exception, should the image fail to load
		 */
		public function __construct(Guild $guild, \stdClass $raid, $background, $type = self::TYPE_PNG)
		{
			if(!($this->ptr = self::load($type, $background)))
			{
				throw new Exception('Unable to load background image');
			}

			$this->guild	= $guild;
			$this->raid	= $raid;

			$this->color('black', 0, 0, 0);
			$this->color('white', 255, 255, 255);
		}

		/**
		 * Deconstructs the image object, causing the image pointer to 
		 * be destroyed as well as all allocated colors.
		 */
		public function __destruct()
		{
			if($this->ptr)
			{
				if($this->colors)
				{
					foreach($this->colors as $color)
					{
						\imagecolordeallocate($this->ptr, $color);
					}
				}

				\imagedestroy($this->ptr);

				$this->ptr = NULL;
			}
		}

		/**
		 * Sets a font
		 *
		 * If a font with that name already exists, then it will be overridden, an empty 
		 * font path value means 'get' the font from the cache.
		 *
		 * Note, if the image pointer is destroyed, then this method will silently return 
		 * without performing anything.
		 *
		 * @param	string			The font name (should be canonical)
		 * @param	string			The path to the font file, must be compatible with GD
		 * @return	void			No value is returned
		 */
		public function font($name, $path = '')
		{
			static $fonts;

			if(!$this->ptr)
			{
				return;
			}
			elseif(!is_array($fonts))
			{
				$fonts = [];
			}

			if(empty($name))
			{
				return;
			}

			if(empty($path))
			{
				if(isset($fonts[$name]))
				{
					return($fonts[$name]);
				}

				return;
			}

			return($fonts[$name] = $path);
		}

		/**
		 * Allocates a color
		 *
		 * Allocates an image color to an image, the image pointer must not be 
		 * destroyed prior to allocating the resource.
		 *
		 * Note, allocation of a color may fail due to various reasons, if you are 
		 * getting an invalid color exception when drawing, it is most likely that 
		 * it failed to allocate at this stage.
		 *
		 * Note, if a color is attempted to be allocated after the image pointer is 
		 * destroyed, then no exception is thrown, to comply with the 'silent' behavior 
		 * of \Wowprogress\Image::font(). Although it can be checked in the return value 
		 * of this method by checking for NULL.
		 *
		 * @param	string			Name of the color (used for deallocation)
		 * @param	integer			The red RBG component (0-255)
		 * @param	integer			The blue RBG component (0-255)
		 * @param	integer			The green RBG component (0-255)
		 * @param	integer			The alpha component (0-127)
		 * @return	integer			Returns the color component value from GD if a color was allocated, and NULL on errors (see above comment)
		 */
		public function color($name, $red = -1, $blue = -1, $green = -1, $alpha = -1)
		{
			if(!$this->ptr)
			{
				return;
			}

			if(empty($name))
			{
				return;
			}

			if($red > -1)
			{
				if($alpha > -1)
				{
					$alloc = \imagecolorallocatealpha($this->ptr, $red, $blue, $green, $alpha);
				}
				else
				{
					$alloc = \imagecolorallocate($this->ptr, $red, $blue, $green);
				}

				if($alloc && $alloc != -1)
				{
					return($this->colors[$name] = $alloc);
				}

				return;
			}
			elseif(isset($this->colors[$name]))
			{
				return($this->colors[$name]);
			}
		}

		/**
		 * Draws a text on an image
		 *
		 * The drawing text is formatted using sprintf() internally, which can be used to 
		 * format the text, such as raid boss killings in the raid currently assigned to this 
		 * class instance:
		 *
		 *   . #1  - %d - %1$d		The number of boss kills (Mythic overrules Heroic, Heroic overrules normal)
		 *   . #2  - %d - %2$d		The number of boss kills on mythic
		 *   . #3  - %d - %3$d		The number of boss kills on heroic
		 *   . #4  - %d - %4$d		The number of boss kills on normal (This number can be off is some are not killed, or if some are only killed on mythic/heroic)
		 *   . #5  - %d - %5$d		The number of bosses in the current raid
		 *   . #6  - %s - %6$s		The current raid name (full name)
		 *   . #7  - %s - %7$s		The current raid name (canonical variant, capitalized)
		 *   . #8  - %s - %8$s		The current mode (Mythic overrules Heroic, Heroic overrules normal)
		 *   . #9  - %s - %9$s		The current mode (shorthand, Mythic overrules Heroic, Heroic overrules normal)
		 *   . #10 - %s	- %10$s		The guild name
		 *   . #11 - %s - %11$s		The guild region shorthand
		 *   . #12 - %s - %12$s		The guild's realm
		 *   . #13 - %d - %13$d		The guild's world rank
		 *   . #14 - %d - %14$d		The guild's region rank
		 *   . #15 - %d - %15$d		The guild's realm rank
		 *
		 * Example values (For 8/12 Heroic down in Throne of Thunder for Paragon on EU-Lightning's Blade (World rank 3, Region rank 3, Realm rank 1)):
		 *
		 *   . #1	8
		 *   . #2	0			(0 because there is no Mythic mode for Throne of Thunder)
		 *   . #3	8
		 *   . #4	12
		 *   . #5	12
		 *   . #6	Throne of Thunder
		 *   . #7	TOT
		 *   . #8	Heroic
		 *   . #9	H
		 *   . #10	Paragon
		 *   . #11	EU
		 *   . #12	Lightning's Blade
		 *   . #13	3
		 *   . #14	3
		 *   . #15	1
		 *
		 * Example to print the raid name and progression (Throne of Thunder: 8/12H):
		 *
		 * <code>
		 * $im->draw('%5$s: %1$d/%4$d%8$d', ...);
		 * </code>
		 *
		 * Note, if the text parameter contains data from the user, then it must be escaped prior 
		 * passing it on to this method to not cause a possible security risk for your system.
		 *
		 * Possible position values are:
		 *
		 *   . WowProgress_Image::POS_TOP_LEFT		Top left
		 *   . WowProgress_Image::POS_TOP_RIGHT		Top right
		 *   . WowProgress_Image::POS_CENTER		Center
		 *   . WowProgress_Image::POS_BOTTOM_LEFT	Bottom left
		 *   . WowProgress_Image::POS_BOTTOM_RIGHT	Bottom right
		 *
		 * Note, that the padding parameter (which defaults to 25 on all sides), are applied to 
		 * the coordinates calculated by the above values, so if a text is not at the exact desired 
		 * position, it can be altered by changing the padding. Padding does not have any effect when 
		 * used inconjuction with the center positioning mode.
		 *
		 * Note, if more text is desired to be written at the same position, the 'array' syntax of the 
		 * padding parameter can be used to cause a margin, saying two texts are gonna be written at the 
		 * center, one below the other, you can give the first one a little padding from the bottom and the 
		 * other one some padding to the top to simulate that, else they will be overlapped.
		 *
		 * Note, if the drop shadow is active (default), then the color of the drop shadow text is painted 
		 * black (RGB: 0, 0, 0).
		 *
		 * Note, if a color is used more than once for drawing multiple lines of text, then it can safely be 
		 * pre-allocated using \Wowprogress\Image::color().
		 *
		 * @param	string			The text to draw on the image, see above for syntax
		 * @param	string			The font to be used, see the WowProgress_Image::font() method
		 * @param	integer			The font size in points
		 * @param	string			The font color to be used, see the \Wowprogress\Image::color() method
		 * @param	integer			The position, the position constants should be used here (\Wowprogress\Image::POS_X)
		 * @param	array|integer		The padding to be applied, if an array is supported it works like in CSS as indexes are mapped in the same order (top, right, bottom, left), defined in pixels
		 * @param	boolean			Whether or not to add a drop-shadow effect (defaults to on)
		 * @return	boolean			Returns true if the text was written, otherwise false
		 *
		 * @throws	\Wowprogress\Exception	Throws an exception, should the image pointer already be destroyed or on invalid fonts/colors
		 *
		 * @uses	\Wowprogress\GuildgetCanonicalRaidName()
		 */
		public function draw($text, $font, $font_size, $font_color, $pos, $padding = 10, $drop_shadow = true)
		{
			static $tf;

			if(!$this->ptr)
			{
				throw new Exception('Image pointer is already destroyed');
			}
			elseif(!($font_path = $this->font($font)))
			{
				throw new Exception('Invalid font');
			}
			elseif((!$color = $this->color($font_color)))
			{
				throw new Exception('Invalid font color');
			}

			if(!$tf)
			{
				$tf = [];

				if(\function_exists('\imageftbbox') && \function_exists('\imagefttext'))
				{
					$tf['bbox'] = '\imageftbbox';
					$tf['text'] = '\imagefttext';
				}
				else
				{
					$tf['bbox'] = '\imagettfbbox';
					$tf['text'] = '\imagettftext';
				}
			}

			$w = \imagesx($this->ptr);
			$h = \imagesy($this->ptr);
			$p = $padding;

			if(!\is_array($padding))
			{
				$p = \array_fill(0, 4, $padding);
			}

			$text = \sprintf($text, ($this->raid->kills->mythic ? $this->raid->kills->mythic : ($this->raid->kills->heroic ? $this->raid->kills->heroic : $this->raid->kills->normal)), $this->raid->kills->mythic, $this->raid->kills->heroic, $this->raid->kills->normal, $this->raid->kills->bosses, $this->raid->name, Guild::getCanonicalRaidName($this->raid->name), ($this->raid->kills->mythic ? 'Mythic' : ($this->raid->kills->heroic ? 'Heroic' : 'Normal')), ($this->raid->kills->mythic ? 'M' : ($this->raid->kills->heroic ? 'HC' : 'N')), \ucwords(\urldecode($this->guild['guild'])), \strtoupper(Guild::getRegionName($this->guild['region'])), \ucfirst(\urldecode($this->guild['realm'])), $this->guild['worldrank'], $this->guild['regionrank'], $this->guild['realmrank']);
			$bbox = $tf['bbox']($font_size, 0, $font_path, $text);

			switch($pos)
			{
				case(self::POS_TOP_LEFT):
				{
					$x = $p[3];
					$y = ($bbox[7] < 0 ? (~$bbox[7] + 1) : $bbox[7]) + $p[1];
				}
				break;
				case(self::POS_TOP_RIGHT):
				{
					$x = $w - $bbox[2] - $p[1];
					$y = ($bbox[7] < 0 ? (~$bbox[7] + 1) : $bbox[7]) + $p[0];
				}
				break;
				case(self::POS_CENTER):
				{
					$x = ($w / 2);
					$y = ($h / 2);

					if($bbox[2] != 0)
					{
						$x -= (($bbox[2] < 0 ? (~$bbox[2] + 1) : $bbox[2]) / 2);
					}

					if($bbox[7] != 0)
					{
						$y += (($bbox[7] < 0 ? (~$bbox[7] + 1) : $bbox[7]) / 2);
					}
				}
				break;
				case(self::POS_BOTTOM_LEFT):
				{
					$x = $p[3];
					$y = $h - $p[2];
				}
				break;
				case(self::POS_BOTTOM_RIGHT):
				{
					$x = $w - ($bbox[2] < 0 ? (~$bbox[2] + 1) : $bbox[2]) - $p[1];
					$y = $h - $p[2];
				}
				break;
				default:
				{
					throw new Exception('Invalid position value');
				}
				break;
			}

			if($drop_shadow)
			{
				if(!$tf['text']($this->ptr, $font_size, 0, $x + 2, $y + 2, $this->color('black'), $font_path, $text))
				{
					return(false);
				}
			}

			return((boolean) $tf['text']($this->ptr, $font_size, 0, $x, $y, $color, $font_path, $text));
		}

		/**
		 * Gets the image pointer
		 *
		 * The technical reasoning for getting the resource using parameters are simple; In PHP 
		 * resources are passed by value, not by reference, this minor hack solves this to unify 
		 * this resource.
		 *
		 * @param	resource		Reference to where the rsrc should be assigned
		 * @return	void			No value is returned
		 */
		public function getPtr(&$rsrc)
		{
			if($this->ptr)
			{
				$rsrc = $this->ptr;
			}
		}

		/**
		 * Saves an image
		 *
		 * This method will always attempt to cause images being saved in the best possible format 
		 * thrus not allowing direct access to the quality or filter parameters in the lower layer 
		 * of gd.
		 *
		 * To apply your own actions to the resource, the WowProgress_Image::getPtr() method can be 
		 * used to fetch the gd image resource pointer.
		 *
		 * Note, the 'quality' applied is as follows:
		 *
		 *   . JPEG	100% Quality
		 *   . PNG	Compression level 9
		 *   . GD2	Default (despite compression for gd2 images are available, using gd image files are not very common)
		 *
		 * Note, formats such as WBMP and XBM will not allocate a foreground color when saving the image, 
		 * use the \Wowprogress\Image::getPtr() to perform that in userland.
		 *
		 * @param	string			The path to where the image should be saved
		 * @param	integer			Optionally a new image type (Defaults to PNG)
		 * @return	void			No value is returned
		 *
		 * @throws	\Wowprogress\Exception	Throws an exception, should and image fail to save
		 */
		public function save($path, $type = self::TYPE_PNG)
		{
			static $supported_types;
			static $quality_options;

			if(!$this->ptr)
			{
				throw new Exception('Image pointer is already destroyed');
			}
			elseif(!$supported_types)
			{
				$supported_types 	= Array();
				$quality_options	= Array(
								self::TYPE_JPEG	=> 100, 
								self::TYPE_PNG	=> 9
								);

				$types_map		= Array(
								self::TYPE_JPEG	=> '\imagejpeg', 
								self::TYPE_PNG	=> '\imagepng', 
								self::TYPE_GIF	=> '\imagegif', 
								self::TYPE_XBM	=> '\imagexbm', 
								self::TYPE_WBMP	=> '\imagewbmp', 
								self::TYPE_GD	=> '\imagegd', 
								self::TYPE_GD2	=> '\imagegd2'
								);

				foreach($types_map as $itype => $function)
				{
					if(\function_exists($function))
					{
						$supported_types[$itype] = $function;
					}
				}
			}

			if(!isset($supported_types[$type]))
			{
				throw new Exception('Invalid image type supplied or not supported: %d', $type);
			}

			if(isset($quality_options[$type]))
			{
				$retval = @$supported_types[$type]($this->ptr, $path, $quality_options[$type]);
			}
			else
			{
				$retval = @$supported_types[$type]($this->ptr, $path);
			}

			if(!$retval)
			{
				throw new Exception('Unable to save image, type: %d, path: %s', $type, $path);
			}
		}

		/**
		 * Loads an image
		 *
		 * Attempts to load an image and return its GD resource id. If an 
		 * Unsupported type of image is loaded, or a wrong type is given 
		 * then an exception is thrown.
		 *
		 * @param	integer			The image type that matches the image
		 * @param	string			The path to the image
		 * @return	resource		Returns a gd image resource id on success, and NULL on failure
		 *
		 * @throws	\Wowprogress\Exception	Throws an exception, should and image fail to load
		 */
		protected static function load($type, $path)
		{
			static $supported_types;

			if(!$supported_types)
			{
				$supported_types 	= [];
				$types_map		= [
								self::TYPE_JPEG	=> '\imagecreatefromjpeg', 
								self::TYPE_PNG	=> '\imagecreatefrompng', 
								self::TYPE_GIF	=> '\imagecreatefromgif', 
								self::TYPE_XBM	=> '\imagecreatefromxbm', 
								self::TYPE_XPM	=> '\imagecreatefromxpm', 
								self::TYPE_WBMP	=> '\imagecreatefromwbmp', 
								self::TYPE_GD	=> '\imagecreatefromgd', 
								self::TYPE_GD2	=> '\imagecreatefromgd2'
								];

				foreach($types_map as $itype => $function)
				{
					if(\function_exists($function))
					{
						$supported_types[$itype] = $function;
					}
				}
			}

			if(!isset($supported_types[$type]))
			{
				throw new Exception('Invalid image type supplied or not supported: %d', $type);
			}

			$im = @$supported_types[$type]($path);

			if(!$im)
			{
				return;
			}

			return($im);
		}
	}
?>