<?php
	/**
	 * WowProgress
	 * =============================================================================
	 *
	 * @author		Kalle Sommer Nielsen 			 <kalle@php.net>
	 * @version		1.6
	 * @copyright		Tuxxedo Software Development 2006+
	 * @license		Apache License, Version 2.0
	 * @package		FOSS
	 * @subpackage		WowProgress
	 *
	 * =============================================================================
	 */


	/**
	 * WowProgress namespace
	 *
	 * This namespace contains all the routines for the Wowprogress data collector 
	 * utility set.
	 *
	 * @author		Kalle Sommer Nielsen <kalle@php.net>
	 * @version		1.6
	 * @package		FOSS
	 * @subpackage		WowProgress
	 */
	namespace Wowprogress;


	/**
	 * Exception class, this class enables a printf-alike syntax for 
	 * the exception constructor
	 *
	 * @author		Kalle Sommer Nielsen <kalle@php.net>
	 * @version		1.6
	 * @package		FOSS
	 * @subpackage		WowProgress
	 */
	class Exception extends \Exception
	{
		/**
		 * Exception constructor
		 *
		 * @param	string			The error message, in a printf-alike formatted string or just a normal string
		 * @param	mixed			Optional argument #n for formatting
		 */
		public function __construct()
		{
			if(!\func_num_args())
			{
				parent::__construct('Unknown error');
			}
			else
			{
				parent::__construct(\call_user_func_array('\sprintf', \func_get_args()));
			}
		}
	}
?>