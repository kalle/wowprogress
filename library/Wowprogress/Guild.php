<?php
	/**
	 * WowProgress
	 * =============================================================================
	 *
	 * @author		Kalle Sommer Nielsen 			 <kalle@php.net>
	 * @version		1.6
	 * @copyright		Tuxxedo Software Development 2006+
	 * @license		Apache License, Version 2.0
	 * @package		FOSS
	 * @subpackage		WowProgress
	 *
	 * =============================================================================
	 */


	/**
	 * WowProgress namespace
	 *
	 * This namespace contains all the routines for the Wowprogress data collector 
	 * utility set.
	 *
	 * @author		Kalle Sommer Nielsen <kalle@php.net>
	 * @version		1.6
	 * @package		FOSS
	 * @subpackage		WowProgress
	 */
	namespace Wowprogress;


	/**
	 * WowProgress data collector for guild progression
	 *
	 * This class uses wowprogress.com to collect progression data, for use in 
	 * applications. The returned data should be cached or stored in a database 
	 * instead of manually fetching updated progression statistics to save both 
	 * execution time and bandwidth on the wowprogress.com end.
	 *
	 * Notes:
	 *  . Data returned here can change at any given time as 
	 *    this class parses the HTML on the guild page to collect 
	 *    the data as wowprogress.com offers no official API, at least 
	 *    for killed bosses which we need.
	 *
	 *  . Returned data only includes the progression for the 
	 *    current expansion, no ratings, and other additional 
	 *    information is returned.
	 *
	 *  . Guild names must be their name associated on WowProgress, 
	 *    meaning that for DREAM Paragon on EU-Lightning's Blade the 
	 *    following values should be used:
	 *
	 *      . WowProgress name / Ingame name: 	DREAM Paragon
	 *      . WowProgress name slug: 		Paragon
	 *
	 *    The 'slug' should be used here, as its the one used in the 
	 *    URL for the guild on WowProgress.com:
	 *
	 *      . http://www.wowprogress.com/guild/eu/lightning-s-blade/Paragon
	 *
	 *  . Supported regions are:
	 *	. EU
	 *	. US
	 *	. KR
	 *	. TW
	 *
	 *    Chineese realms (CN/ZH) are not currently available at 
	 *    wowprogress.com.
	 *
	 *  . Realm and guild names are subject to normalization in the 
	 *    form of converting to lower cased values, removing ' and 
	 *    other special characters, and replace spaces with a hyphen.
	 *
	 *    Such values should be UTF-8 or ISO-8859-1 compatible as they 
	 *    will be used to generate the URL from where the data are 
	 *    fetched, in the conversation provided to the class as no 
	 *    normalization is used.
	 *
	 *  . Exceptions thrown by this class can contain debuggable 
	 *    information and such, so it is recommeded not to publicly 
	 *    display their full error messages if an error should occur.
	 *
	 *  . Some guilds may encounter parsing issues, these only really 
	 *    happens when a tier that was not added to Wowprogress. This 
	 *    means that proper scraping cannot be done. Once a page is 
	 *    correctly looking at Wowprogress, the script should start 
	 *    to function once again.
	 *
	 *  . Support for different tiers are available, currently the 
	 *    following are available:
	 *
	 *      . T11 (Cataclysm): 				BWD, BOT, TOTFW
	 *      . T12 (Cataclysm): 				FL
	 *      . T13 (Cataclysm): 				DS
	 *	. T14 (Mists of Pandaria): 			MSV, HOF, TOES
	 *	. T15 (Mists of Pandaria):			TOT
	 *      . T16 (Mists of Pandaria):			SOO
	 *      . T17 (Warlords of Draenor):			HM, BF
	 *      . T18 (Warlords of Draenor):			HC
	 *
	 *    Despite older tiers are available at Wowprogress.com, these 
	 *    tiers were split in 10M and 25M mode and progression is 
	 *    based on achievements, the vast majority does not care about 
	 *    this data anyway.
	 *
	 *  . Some raids uses a more in-game common canonical name than that 
	 *    of the armory, these are:
	 *
	 *      . Blackwing Descent (Cataclysm): 		BD > BWD
	 *      . The Bastion of Twilight (Cataclysm): 		BT > BOT
	 *      . Mogu'Shan Vaults (Mists of Pandaria): 	MSV > MV
	 *      . Siege of Orgrimmar (Mists of Pandaria):	SOO > SO
	 * 	. Blackrock Foundry (Warlords of Draenor):	BF > BRF
	 *      . Hellfire Citadel (Warlords of Draenor):	HC > HFC
	 *
	 *  . Some raids have heroic-mode only bosses, the following raids that 
	 *    have that is:
	 *
	 *      . The Bastion of Twilight (Cataclysm)
	 *	. Throne of Thunder (Mists of Pandaria)
	 *
	 *    These raids will display a different number of total bosses on 
	 *    normal mode compared to the heroic mode.
	 *
	 *  . As of Warlords of Draenor, Mythic mode was added and the following 
	 *    raids supports this mode:
	 *
	 *      . Highmaul (Warlords of Draenor)
	 *      . Blackrock Foundry (Warlords of Draenor)
	 *      . Hellfire Citadel (Warlords of Draenor)
	 *
	 *    Note, for tiers without Mythic mode, the mythic kill count will 
	 *    always be 0, and therefore should favour Heroic over such.
	 *
	 *  . On new releases of raids, some guilds may experience a read error 
	 *    when trying fetch new information, once WowProgress updates for 
	 *    that paticular guild, it will be instantly reflected in the program.
	 *
	 * @author		Kalle Sommer Nielsen <kalle@php.net>
	 * @version		1.6
	 * @package		FOSS
	 * @subpackage		WowProgress
	 */
	class Guild implements \ArrayAccess
	{
		/**
		 * Region constant - Europe (EU)
		 *
		 * @var 	integer
		 */
		const REGION_EU					= 1;

		/**
		 * Region constant - America / Oceanic (US)
		 *
		 * @var		integer
		 */
		const REGION_US					= 2;

		/**
		 * Region constant - Korea (KR)
		 *
		 * @var		integer
		 */
		const REGION_KR					= 3;

		/**
		 * Region constant - Taiwan (TW)
		 *
		 * @var		integer
		 */
		const REGION_TW					= 4;


		/**
		 * Tiers supported by WowProgress
		 *
		 * @var		array
		 */
		protected static $tiers				= [
									18, 
									17, 
									16, 
									15, 
									14, 
									13, 
									12, 
									11
									];

		/**
		 * Region mapping (for numeric values)
		 *
		 * @var		array
		 */
		protected static $region_map			= [
									self::REGION_EU 		=> 'eu', 
									self::REGION_US			=> 'us', 
									self::REGION_KR 		=> 'kr', 
									self::REGION_TW 		=> 'tw'
									];

		/**
		 * Raid name rewrites, some raid names uses other (and probably) 
		 * more common canonical names, this array contains a list of such 
		 * for conversation in the WowProgress:loadTiers() method
		 *
		 * @var		array
		 */
		protected static $raid_rewrites			= [
									'Blackwing Descent'		=> 'bwd', 
									'The Bastion of Twilight'	=> 'bot', 
									'Firelands'			=> 'fl', 
									'Mogu\'Shan Vaults'		=> 'mv', 
									'Siege of Ogrimmar'		=> 'so', 
									'Blackrock Foundtry'		=> 'brf', 
									'Highmaul'			=> 'hm', 
									'Hellfire Citadel'		=> 'hfc'
									];

		/**
		 * Some raids have Heroic mode only bosses, WowProgress seems to 
		 * expose this to normal mode progression. This structure holds 
		 * data to cut off that extra (unkillable boss) for normal 
		 * modes
		 *
		 * Currently the following raids have Heroic-only mode bosses:
		 *
		 *   . The Bastion of Twilight:		Sinestra
		 *   . Throne of Thunder: 		Ra-den
		 *
		 * Each value contains an index with relevant information on how 
		 * to properly display the raids. The indexes are as follows:
		 *
		 *   . count:				The number bosses available on normal mode
		 *   . bosses:				The bosses that are heroic-only mode
		 *
		 * @var		array
		 */
		protected static $raid_hconly			= [
									'bot'				=> [
														'count'		=> 4, 
														'bosses'	=> [
																	'Sinestra'
																	]
														], 
									'tot'				=> [
														'count'		=> 12, 
														'bosses'	=> [
																	'Ra-den'
																	]
														]
									];

		/**
		 * Mythic only raids, these were added starting Warlords of Draenor
		 * and have a fixed raid size.
		 *
		 * Currently the following raids have Mythic mode bosses:
		 *
		 *   . Highmaul (Warlords of Draenor)
		 *   . Blackrock Foundtry (Warlords of Draenor)
		 *   . Hellfire Citadel (Warlords of Draenor)
		 *
		 * @var		array
		 */
		protected static $raid_mythiconly 		= [
									'hm', 
									'bf', 
									'hfc'
									];


		/**
		 * Progression data
		 *
		 * @var		array
		 */
		protected $progression				= [];

		/**
		 * Guild information
		 *
		 * @var		array
		 */
		protected $guild_info				= [
									'region'			=> '', 
									'realm'				=> '', 
									'guild'				=> '', 
									'worldrank'			=> 0, 
									'regionrank'			=> 0, 
									'realmrank'			=> 0
									];

		/**
		 * Contains the loaded tier references for loaded tiers
		 *
		 * @var		array
		 */
		protected $tier_refs				= [];


		/**
		 * Constructs a new progression object for a guild
		 *
		 * Note, this function alters the internals to the LibXML error 
		 * handling:
		 *
		 *  . Clears the error buffer (libxml_clear_errors())
		 *  . Turns on internal error handling (libxml_use_internal_errors())
		 *
		 * This class does not reset the behaviour or clears its buffer after 
		 * use, to make more extensive debugging possible if desired.
		 *
		 * To get expansion specific tiers, say for example Cataclysm, the 4th 
		 * parameter can be used to tell which tiers you want fetched:
		 *
		 * <code>
		 * $cataclysm_guild = new \Wowprogress\Guild(..., ..., ..., '11,12,13');
		 * </code>
		 *
		 * Available tiers that can be fetched can be found using: \Wowprogress\Guild::getTiers()
		 * and loaded tiers as WowProgress:getLoadedTiers()
		 *
		 * @param	integer			The region (one of the \Wowprogress\Guild::REGION_XXX constants)
		 * @param	string			Realm name
		 * @param	string			Guild name
		 * @param	string			Tiers in a comma separated list (defaults to current expansion tiers), or * for everything
		 *
		 * @throws	\Wowprogress\Exception	Throws a general exception, in case of an error
		 */
		public function __construct($region, $realm, $guild, $tiers = '18')
		{
			if(!self::$region_map[$region])
			{
				throw new \WowProgress_Exception('Invalid region name id: %s', $region);
			}

			$this->guild_info['region']	= (integer) $region;
			$this->guild_info['realm']	= self::normalize($realm, true);
			$this->guild_info['guild']	= self::normalize($guild);

			\libxml_clear_errors();
			\libxml_use_internal_errors(true);

			if($tiers)
			{
				$this->getParsedTiers($tiers);
			}

			if(empty($this->guild_info['worldrank']) || empty($this->guild_info['realmrank']))
			{
				self::getParsedRanks(@\file_get_contents('http://www.wowprogress.com/guild/' . self::$region_map[$this->guild_info['region']] . '/' . $this->guild_info['realm'] . '/' . $this->guild_info['guild']), $this->guild_info);
			}
		}

		/**
		 * Get property overloader
		 *
		 * This methods overloads the default get property overloader 
		 * to allow the shorthand syntax below to fetch raid progression 
		 * data:
		 *
		 * <code>
		 * $guild->mv->...
		 * </code>
		 *
		 * Specific tiers can also be used using overloading:
		 *
		 * <code>
		 * $guild->t11->bot->...
		 * </code>
		 *
		 * Overloadable raids depends on the loaded tiers, see the initial 
		 * comment for the WowProgress class for more detailed tier 
		 * information.
		 *
		 * @param	string			The canonical raid name
		 * @return	array			Returns an array with the raid data, and false if an invalid (or none raid name) is supplied
		 */
		public function __get($name)
		{
			if(!isset($this->progression[$name]))
			{
				if($name{0} == 't')
				{
					$tier = \substr($name, 1);

					if(isset($this->tier_refs[$tier]))
					{
						return($this->tier_refs[$tier]);
					}
				}

				return(false);
			}

			return($this->progression[$name]);
		}

		/**
		 * Gets the current loaded raids canonical name, like MSV, HOF, 
		 * etc.
		 *
		 * These names are in returned as lower cased and their length 
		 * varies depending on the value of \Wowprogress\Guild::getCanonicalRaidName().
		 *
		 * @return	array			Returns an array of loaded raid names (usually lower cased), this array may be empty if none is loaded
		 */
		public function getLoadedRaids()
		{
			return(\array_keys($this->progression));
		}

		/**
		 * Gets the loaded tier ids
		 *
		 * @return	array			Returns an array with the loaded tier information, this array may be empty if none is loaded
		 */
		public function getLoadedTiers()
		{
			return(\array_keys($this->tier_refs));
		}

		/**
		 * Gets a specific loaded tier
		 *
		 * This is the same as using overloading:
		 *
		 * <code>
		 * ...
		 *
		 * $t13 = $guild->t13;
		 * $t13 = $guild->getTier(13);
		 *
		 * ...
		 * </code>
		 *
		 * @param	integer			The tier to get
		 * @return	array			Returns an array with the raids loaded within that tier. Returns false on invalid/not loaded tiers.
		 */
		public function getTier($tier)
		{
			if(isset($this->tier_refs[$tier]))
			{
				return($this->tier_refs[$tier]);
			}

			return(false);
		}

		/**
		 * Gets tiers that can be used together with this class
		 *
		 * @return	array			Returns an array with numeric indices of available tiers
		 */
		public static function getTiers()
		{
			return(self::$tiers);
		}

		/**
		 * Normalization method, this normalizes things like realm and 
		 * guild names.
		 *
		 * Multibyte data, such as guild names from Korea and Taiwan 
		 * should pass here, as the PHP functions we use internally 
		 * uses a form of internal normalization.
		 *
		 * This function will do the following operation on the string:
		 *
		 *  . Convert to lower case
		 *  . Replace ' with -
		 *  . Replace spaces with a + (replaces with a - if $url_combat is true)
		 *
		 * @param	string			The string to normalize
		 * @param	boolean			Whether or not to enable url compatiblity (see above), does not comply with RFC 3986
		 * @return	string			The normalized string
		 */
		public static function normalize($input, $url_compat = false)
		{
			return(\str_replace(['\'', ' '], ['-', ($url_compat ? '-' : '+')], \strtolower($input)));
		}

		/**
		 * Converts a full raid name into a canonical name (lower cased)
		 *
		 * <code>
		 * echo 'Heart of Fear\'s shorthand name is: ' . \Wowprogress\Guild::getCanonicalRaidName('Heart of Fear');
		 * </code>
		 *
		 * @param	string			The raid name
		 * @return	string			Returns the canonical raid name
		 */
		public static function getCanonicalRaidName($raid)
		{
			$indices = \explode(' ', \str_replace('\'', ' ', \strtolower($raid)));

			if(\sizeof($indices) === 1)
			{
				return($indices[0]{0});
			}

			$canonical_name = '';

			foreach($indices as $index)
			{
				$canonical_name .= $index{0};
			}

			return($canonical_name);
		}

		/**
		 * Gets the verbose (shorthand name) of a region based on its numeric constant
		 *
		 * @param	integer			The region code (one of the WowProgress::REGION_XXX constants)
		 * @return	string			Returns the shorthand name on success (2 characters), and false on failure
		 */
		public static function getRegionName($code)
		{
			return((isset(self::$region_map[$code]) ? self::$region_map[$code] : false));
		}

		/**
		 * ArrayAccess method, check if an offset exists (guild info)
		 *
		 * @param	scalar			The offset
		 * @return	boolean			Returns true if the offset exists, otherwise false
		 */
		public function offsetExists($offset)
		{
			return(isset($this->guild_info[$offset]));
		}

		/**
		 * ArrayAccess method, gets an offset value (guild info)
		 *
		 * @param	scalar			The offset
		 */
		public function offsetGet($offset)
		{
			return((isset($this->guild_info[$offset]) ? $this->guild_info[$offset] : NULL));
		}

		/**
		 * ArrayAccess method, sets an offset to a new value (have no effect)
		 *
		 * @param	scalar			The offset
		 * @param	mixed			The new value
		 * @return	void			No value is returned
		 */
		public function offsetSet($offset, $value)
		{
		}

		/**
		 * ArrayAccess method, deletes an offset (have no effect)
		 *
		 * @param	scalar			The offset
		 * @return	void			No value is returned
		 */
		public function offsetUnset($offset)
		{
		}

		/**
		 * Loads and parses the progression information based on tiers
		 *
		 * This method is the internal parser for stripping down the HTML code 
		 * from the WowProgress.com website and can be used to fetch multiple 
		 * tiers.
		 *
		 * @param	string			Tiers in a comma separated list (for example: 11,12,13), or * for everything
		 * @return	void			No value is returned
		 *
		 * @throws	\Wowprogress\Exception	Throws a general exception, in case of an error
		 */
		public function getParsedTiers($tiers)
		{
			static $kills, $ptr, $tier_offsets, $progress_type;

			if(!$kills)
			{
				$kills		= new \stdClass;
				$kills->none	= 0;
				$kills->normal	= 0;
				$kills->heroic	= 0;
				$kills->mythic	= 0;
				$kills->bosses	= 0;

				$ptr 		= new \stdClass;
				$ptr->name	= '';
				$ptr->counter	= 0;
				$ptr->parsing	= false;
				$ptr->heroic	= false;
				$ptr->mythic	= false;
				$ptr->bosses	= [];
				$ptr->tier	= 0;

				$tier_offsets	= [
							18		=> 45, 
							17		=> 45, 
							16		=> 45, 
							15		=> 44, 
							14		=> 43, 
							13		=> 41, 
							12		=> 41, 
							11		=> 41
							];

				$progress_type	= [
							'none'		=> -1, 
							'normal'	=> 0, 
							'heroic'	=> 1, 
							'mythic'	=> 2
							];
			}

			if($tiers == '*')
			{
				$requested_tiers = self::$tiers;
			}
			else
			{
				$requested_tiers 	= [];
				$tiers 			= \explode(',', $tiers);

				if(!$tiers)
				{
					return;
				}

				foreach($tiers as $tier)
				{
					if(\in_array($tier, self::$tiers))
					{
						$requested_tiers[] = (integer) $tier;
					}
				}

				if(!$requested_tiers)
				{
					return;
				}

				\rsort($requested_tiers);
			}

			foreach($requested_tiers as $tier)
			{
				$url 	= 'http://www.wowprogress.com/guild/' . self::$region_map[$this->guild_info['region']] . '/' . $this->guild_info['realm'] . '/' . $this->guild_info['guild'] . '/rating.tier' . $tier;
				$html	= @\file_get_contents($url);

				if(!$html)
				{
					throw new Exception('Unable to load data from URL: %s', $url);
				}
				elseif(($spos = \strpos($html, '<div id="data_')) === false || ($epos = \strpos($html, '<br/></div></div>', $spos)) === false)
				{
					throw new \Exception('Parsing failed, could not find syntax identifier at T%d', $tier);
				}

				if($tier === self::$tiers[0])
				{
					self::getParsedRanks($html, $this->guild_info);
				}

				if(!isset($tier_offsets[$tier]))
				{
					throw new Exception('Don\'t know how to parse this tier: T%d', $tier);
				}

				$xml = @\simplexml_load_string('<?xml version="1.0"?><raids>' . \substr($html, $spos + (\strpos($html, '<div style="', $spos) - $spos), $epos - $spos - $tier_offsets[$tier]) . '</raids>');

				if(!$xml)
				{
					throw new Exception('Unable to re-parse XML: %s', \libxml_get_last_error()->message);
				}

				$iptr 	= NULL;
				$raids	= [];

				foreach($xml as $tag)
				{
					switch(\strtolower($tag->getName()))
					{
						case('div'):
						{
							if($iptr && $iptr->parsing)
							{
								$this->progression[$iptr->name]			= new \stdClass;
								$this->progression[$iptr->name]->name		= $iptr->raid;
								$this->progression[$iptr->name]->tier		= $iptr->tier;
								$this->progression[$iptr->name]->kills		= $iptr->kills;
								$this->progression[$iptr->name]->kills->bosses	= $iptr->counter;
								$this->progression[$iptr->name]->bosses		= $iptr->bosses;
								$this->progression[$iptr->name]->heroic		= $iptr->heroic;
								$this->progression[$iptr->name]->mythic		= $iptr->mythic;
							}

							$tag = (string) $tag;

							if(isset(self::$raid_rewrites[$tag]))
							{
								$raid = self::$raid_rewrites[$tag];
							}
							else
							{
								$raid = self::getCanonicalRaidName($tag);
							}

							$iptr 		= clone $ptr;
							$iptr->name	= $raids[] = $raid;
							$iptr->raid	= $tag;
							$iptr->parsing	= true;
							$iptr->kills	= clone $kills;
							$iptr->tier	= $tier;
						}
						break;
						case('span'):
						{
							if(!$iptr || !$iptr->parsing)
							{
								continue;
							}

							$name 	= $tag;
							$mythic	= (\substr((string) $name, 0, 2) === 'M:');
							$heroic = (\substr((string) $name, 0, 2) === 'H:');

							if(($pos = \strpos($name, ': ')) !== false)
							{
								$name = \rtrim(substr($name, $pos + 2));
							}

							if(($pos = \strpos($name, '(')) !== false)
							{
								$name = \substr($name, 0, $pos - 1);
							}

							$boss			= new \stdClass;
							$boss->name		= $name;
							$boss->progress		= ($mythic ? 'mythic' : (\in_array($iptr->name, self::$raid_mythiconly) && $heroic ? 'heroic' : \strtolower(\substr($tag['class'], 9))));
							$boss->progress_type	= $progress_type[$boss->progress];
							$boss->is_hconly	= (isset(self::$raid_hconly[$iptr->name]) && \in_array($name, self::$raid_hconly[$iptr->name]['bosses']));

							if(!$iptr->heroic && $boss->progress == 'heroic')
							{
								$iptr->heroic = true;
							}
							elseif(!$iptr->mythic && $boss->progress == 'mythic' && \in_array($iptr->name, self::$raid_mythiconly))
							{
								$iptr->mythic = true;
							}

							$iptr->bosses[]	= $boss;

							++$iptr->counter;
							++$iptr->kills->{$boss->progress};
						}
						break;
					}
				}

				if($iptr && $iptr->parsing)
				{
					$this->progression[$iptr->name]			= new \stdClass;
					$this->progression[$iptr->name]->name		= $iptr->raid;
					$this->progression[$iptr->name]->tier		= $iptr->tier;
					$this->progression[$iptr->name]->kills		= $iptr->kills;
					$this->progression[$iptr->name]->kills->bosses	= $iptr->counter;
					$this->progression[$iptr->name]->bosses		= $iptr->bosses;
					$this->progression[$iptr->name]->heroic		= $iptr->heroic;
					$this->progression[$iptr->name]->mythic		= $iptr->mythic;
				}

				if($raids)
				{
					foreach($raids as $raid)
					{
						if(!isset($this->tier_refs[$tier]))
						{
							$this->tier_refs[$tier] = new \stdClass;
						}

						$this->tier_refs[$tier]->{$raid} = &$this->progression[$raid];

						if(!$this->progression[$raid]->heroic && isset(self::$raid_hconly[$raid]))
						{
							$diff = $this->progression[$raid]->kills->bosses - self::$raid_hconly[$raid]['count'];

							if($diff)
							{
								do
								{
									\array_pop($this->progression[$raid]->bosses);
								}
								while(--$diff);
							}

							$this->progression[$raid]->kills->bosses = self::$raid_hconly[$raid];
						}
					}
				}
			}
		}

		/**
		 * Parses ranks from a HTML chunk
		 *
		 * This should only be called for the latest tier, meaning the top tier listed in 
		 * the \Wowprogress\Guild::$tiers array.
		 *
		 * Second and third parameter are passed by reference to this method, so they are 
		 * directly allocated into the guild struct.
		 *
		 * @param	string			The HTML to parse from
		 * @param	integer			Reference to the guild information struct
		 * @return	void			No value is returned
		 */
		protected static function getParsedRanks($html, &$guild_info)
		{
			if(($spos = \strpos($html, '<a href="/pve/world">World</a>')) !== false && ($epos = \strpos($html, '</span>', $spos)) !== false)
			{
				$dump = \substr($html, $spos, $epos - $spos);

				if(($spos = \strrpos($dump, '>')) !== false)
				{
					$guild_info['worldrank'] = (integer) \substr($dump, $spos + 1);
				}
			}

			if(($spos = \strpos($html, '<a href="/pve/' . ($region = self::$region_map[$guild_info['region']]) . '">' . \strtoupper($region) . '</a>')) !== false && ($epos = \strpos($html, '</span>', $spos)) !== false)
			{
				$dump = \substr($html, $spos, $epos - $spos);

				if(($spos = \strrpos($dump, '>')) !== false)
				{
					$guild_info['regionrank'] = (integer) \substr($dump, $spos + 1);
				}
			}

			if(($spos = \strpos($html, 'Realm</a>: </td><td>')) !== false && ($epos = \strpos($html, '</td>', $spos + 20)) !== false)
			{
				$guild_info['realmrank'] = (integer) \substr($html, $spos + 20, $epos - $spos - 20);
			}
		}
	}
?>