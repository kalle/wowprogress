<?php
	/**
	 * WowProgress
	 * =============================================================================
	 *
	 * @author		Kalle Sommer Nielsen 			 <kalle@php.net>
	 * @version		1.6
	 * @copyright		Tuxxedo Software Development 2006+
	 * @license		Apache License, Version 2.0
	 * @package		FOSS
	 * @subpackage		WowProgress
	 *
	 * =============================================================================
	 */


	/**
	 * Namespace alias imports
	 */
	use Wowprogress\Exception;
	use Wowprogress\Guild;
	use Wowprogress\Image;


	/**
	 * Quick and dirty autoloader
	 */
	spl_autoload_register(function($object)
	{
		if(DIRECTORY_SEPARATOR === '/')
		{
			$object = str_replace('\\', '/', $object);
		}

		if(!is_file('./library/' . $object . '.php'))
		{
			exit('Error: Cannot load object: ' . $object);
		}

		require('./library/' . $object . '.php');
	});


	session_start();

	$predefined = [
			1	=> [
					'region'	=> 1, 
					'realm'		=> 'Grim Batol', 
					'guild'		=> 'Project Hollywood', 
					'tiers'		=> '*'
					], 
			2	=> [
					'region'	=> 1, 
					'realm'		=> 'Twisting Nether', 
					'guild'		=> 'Serenity', 
					'tiers'		=> '*'
					], 
			3	=> [
					'region'	=> 1, 
					'realm'		=> 'Lightning\'s Blade', 
					'guild'		=> 'Paragon', 
					'tiers'		=> '*'
					],  
			4	=> [
					'region'	=> 1, 
					'realm'		=> 'Silvermoon', 
					'guild'		=> 'After Hours', 
					'tiers'		=> '*'
					]
			];

	if(isset($_POST['id']))
	{
		$_GET['id'] = $_POST['id'];
	}

	if(isset($_GET['id']) && isset($predefined[(integer) $_GET['id']]))
	{
		$_SESSION['region']	= $predefined[(integer) $_GET['id']]['region'];
		$_SESSION['realm']	= $predefined[(integer) $_GET['id']]['realm'];
		$_SESSION['guild']	= $predefined[(integer) $_GET['id']]['guild'];
		$_SESSION['tiers']	= $predefined[(string) $_GET['id']]['tiers'];
	}

	if(!isset($_SESSION['realm']) || !isset($_SESSION['guild']) || !isset($_SESSION['region']) || !isset($_SESSION['tiers']) || (isset($_GET['setguild']) && !isset($_POST['id'])))
	{
		if(!isset($_POST['realm']) || !isset($_POST['guild']) || !isset($_POST['region']) || !isset($_POST['tiers']))
		{
			echo '<fieldset style="width: 30%;">';
			echo '<legend><h3>Load a predefined guild</h3></legend>';
			echo '<form action="./?setguild=true" method="post">';
			echo '<table>';
			echo '<tr>';
			echo '<td><strong>Guild:</strong></td>';
			echo '<td><select name="id">';

			foreach($predefined as $id => $guild)
			{
				echo '<option value="' . $id . '">' . $guild['guild'] . ' on ' . $guild['realm'] . '</option>';
			}

			echo '</select></td>';
			echo '</tr>';
			echo '<tr>';
			echo '<td colspan="2"><input type="submit" value="Continue" /></td>';
			echo '</tr>';
			echo '</table>';
			echo '</form>';
			echo '</fieldset>';

			echo '<fieldset style="width: 30%;">';
			echo '<legend><h3>Set custom guild</h3></legend>';
			echo '<form action="./?setguild=true" method="post">';
			echo '<table>';
			echo '<tr>';
			echo '<td><strong>Region:</strong></td>';
			echo '<td><select name="region">';
			echo '<option value="1"' . (isset($_SESSION['region']) && $_SESSION['region'] == 1 ? ' selected="selected"' : '') . '>Europe (EU)</option>';
			echo '<option value="2"' . (isset($_SESSION['region']) && $_SESSION['region'] == 2 ? ' selected="selected"' : '') . '>United States (US)</option>';
			echo '<option value="3"' . (isset($_SESSION['region']) && $_SESSION['region'] == 3 ? ' selected="selected"' : '') . '>Korea (KR)</option>';
			echo '<option value="4"' . (isset($_SESSION['region']) && $_SESSION['region'] == 4 ? ' selected="selected"' : '') . '>Taiwan (TW)</option>';
			echo '</select></td>';
			echo '</tr>';
			echo '<tr>';
			echo '<td><strong>Realm:</strong></td><td><input type="text" name="realm"' . (isset($_SESSION['realm']) ? ' value="' . htmlspecialchars($_SESSION['realm'], ENT_QUOTES) . '"' : '') . ' /></td>';
			echo '</tr>';
			echo '<tr>';
			echo '<td><strong>Guild:</strong></td><td><input type="text" name="guild"' . (isset($_SESSION['guild']) ? ' value="' . htmlspecialchars($_SESSION['guild'], ENT_QUOTES) . '"' : '') . ' /></td>';
			echo '</tr>';
			echo '<tr>';
			echo '<td><strong>Tiers:</strong></td><td><input type="text" name="tiers" value="' . (isset($_SESSION['tiers']) ? htmlspecialchars($_SESSION['tiers'], ENT_QUOTES) : '18') . '" /></td>';
			echo '</tr>';
			echo '<tr>';
			echo '<td colspan="2"><input type="submit" value="Continue" /></td>';
			echo '</tr>';
			echo '</table>';
			echo '</form>';
			echo '</fieldset>';

			exit;
		}
		else
		{
			$_SESSION['region'] 	= (integer) $_POST['region'];
			$_SESSION['realm'] 	= (string) $_POST['realm'];
			$_SESSION['guild'] 	= (string) $_POST['guild'];
			$_SESSION['tiers']	= (string) $_POST['tiers'];
		}
	}

	try
	{
		$progress 	= new Guild($_SESSION['region'], $_SESSION['realm'], $_SESSION['guild'], $_SESSION['tiers']);
		$raids		= $progress->getLoadedRaids();
	}
	catch(Exception $e)
	{
		die('Error: ' . $e->getMessage());
	}

	echo '<strong>Mode</strong>: ';
	echo '<a href="./">Simple</a> | ';
	echo '<a href="./?advanced=true">Advanced</a> | ';
	echo '<a href="./?images=true">Images</a> | ';
	echo '<a href="./?setguild=true">Change guild</a>';
	echo '<h1>Progression for \'' . ucwords(urldecode($progress['guild'])) . '\' on ' . strtoupper(urldecode(Guild::getRegionName($progress['region']))) . '-' . ucfirst($progress['realm']) . '</h1>';
	echo '<p><strong>World rank:</strong> ' . $progress['worldrank'] . '</p>';
	echo '<p><strong>Region rank:</strong> ' . $progress['regionrank'] . '</p>';
	echo '<p><strong>Realm rank:</strong> ' . $progress['realmrank'] . '</p>';

	if(isset($_GET['advanced']))
	{
		foreach($raids as $raid_name)
		{
			$raid = $progress->{$raid_name};

			echo '<h2>' . $raid->name . ' (<small>T' . $raid->tier . '</small>): ' . ($raid->kills->mythic ? $raid->kills->mythic : ($raid->kills->heroic ? $raid->kills->heroic : $raid->kills->normal)) . '/' . $raid->kills->bosses . ' ' . ($raid->kills->mythic ? 'M' : ($raid->kills->heroic ? 'HC' : 'N')) . '</h2>';
			echo '<table border="1">';
			echo '<thead>';
			echo '<tr>';
			echo '<th><strong>Boss</strong></th>';
			echo '<th><strong>Normal</strong></th>';
			echo '<th><strong>Heroic</strong></th>';

			if($raid->mythic)
			{
				echo '<th><strong>Mythic</strong></th>';
			}

			echo '</tr>';
			echo '</thead>';
			echo '<tbody>';

			foreach($raid->bosses as $boss)
			{
				echo '<tr>';
				echo '<td>' . $boss->name . '</td>';
				echo '<td>' . ($boss->progress_type >= 0 && !$boss->is_hconly ? '&#x2713;' : '&nbsp') . '</td>';
				echo '<td>' . ($boss->progress_type >= 1 ? '&#x2713;' : '&nbsp') . '</td>';

				if($raid->mythic)
				{
					echo '<td>' . ($boss->progress_type >= 2 ? '&#x2713;' : '&nbsp') . '</td>';
				}

				echo '</tr>';
			}

			echo '</tbody>';
			echo '</table>';
		}
	}
	elseif(isset($_GET['images']))
	{
		/*
		date_default_timezone_set('UTC');

		$timenow = time();
		*/

		foreach($raids as $raid_name)
		{
			$raw_image = './resources/images/raid_' . $raid_name . '.jpg';

			echo '<h2>' . $progress->{$raid_name}->name . '</h2>';

			if(!is_file($raw_image))
			{
				echo '<p>This raid does not have a background image available</p>';

				continue;
			}

			$img_file = './resources/images/cache/progress_' . $raid_name . '.jpg';

			if(1 || !is_file($img_file) || $timenow < (@filemtime($img_file) + 3600))
			{
				$im = new Image($progress, $progress->{$raid_name}, $raw_image, Image::TYPE_JPEG);

				$im->font('basic', './resources/fonts/Calibri.ttf');

				$im->draw('%10$s', 'basic', 20, 'white', \Wowprogress\Image::POS_TOP_LEFT);
				$im->draw('%1$d/%5$d %8$s', 'basic', 15, 'white', \Wowprogress\Image::POS_BOTTOM_RIGHT);

				$im->save($img_file);
			}

			echo '<p><img src="' . $img_file . '" /></p>';
		}
	}
	else
	{
		foreach($raids as $raid_name)
		{
			$raid = $progress->{$raid_name};

			echo '<h2>' . strtoupper($raid_name) . ': ' . ($raid->kills->mythic ? $raid->kills->mythic : ($raid->kills->heroic ? $raid->kills->heroic : $raid->kills->normal)) . '/' . $raid->kills->bosses . ' ' . ($raid->kills->mythic ? 'M' : ($raid->kills->heroic ? 'HC' : 'N')) . '</h2>';
		}
	}
?>